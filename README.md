**Malin** is a dark [Firefox](https://www.mozilla.org/en-US/firefox/new) theme. Download it [here](https://addons.mozilla.org/en-US/firefox/addon/malin).

---

**By [Avanier](https://avanier.now.sh).**
